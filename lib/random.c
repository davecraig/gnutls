/*
 * Copyright (C) 2008-2012 Free Software Foundation, Inc.
 *
 * Author: Nikos Mavrogiannopoulos
 *
 * This file is part of GnuTLS.
 *
 * The GnuTLS is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 *
 */

/* This file handles all the internal functions that cope with random data.
 */

#include "gnutls_int.h"
#include "errors.h"
#include <random.h>
#include "locks.h"
#include <fips.h>

#include "pthread.h"

#if defined(FUZZING_BUILD_MODE_UNSAFE_FOR_PRODUCTION)
extern gnutls_crypto_rnd_st _gnutls_fuzz_rnd_ops;
#endif

static pthread_key_t ctxt;
static bool ctxt_error = false;

static void *rnd_createctxt(void)
{
    void * new_context = 0;
    if (_gnutls_rnd_ops.init(&new_context) < 0)
        gnutls_assert();

  return new_context;
}

static void rnd_destroyctxt(void *ctx)
{
    if (_gnutls_rnd_ops.deinit != NULL)
        _gnutls_rnd_ops.deinit(ctx);
}

static void *rnd_getctxt(void)
{
    if(ctxt_error)
        return NULL;

    /* See if we already have a context on this thread and return it if we do*/
    void * res = NULL;
    res = pthread_getspecific(ctxt);
    if (res != NULL) return res;

    /* Create a new context on this thread */
    res = rnd_createctxt();
    if (res == NULL) return NULL;

    /* Store it */
    if (pthread_setspecific(ctxt, res) < 0)
    {
        rnd_destroyctxt(res);
        return NULL;
    }

    /* And return the new context */
    return res;
}

int _gnutls_rnd_preinit(void)
{
    int ret;

#if defined(FUZZING_BUILD_MODE_UNSAFE_FOR_PRODUCTION)
# warning Insecure PRNG is enabled
    ret = gnutls_crypto_rnd_register(100, &_gnutls_fuzz_rnd_ops);
    if (ret < 0)
        return ret;

#elif defined(ENABLE_FIPS140)
    /* The FIPS140 random generator is only enabled when we are compiled
     * with FIPS support, _and_ the system requires FIPS140.
     */
    if (_gnutls_fips_mode_enabled() == 1) {
        ret = gnutls_crypto_rnd_register(100, &_gnutls_fips_rnd_ops);
        if (ret < 0)
            return ret;
    }
#endif

    ret = _rnd_system_entropy_init();
    if (ret < 0) {
        gnutls_assert();
        return GNUTLS_E_RANDOM_FAILED;
    }

    /* Create a key for our thread local storage, along with a destructor
    function to be called when the thread is destroyed */
    if (pthread_key_create(&ctxt, &rnd_destroyctxt) < 0)
        ctxt_error = true;

    return 0;
}

void _gnutls_rnd_deinit(void)
{
    _rnd_system_entropy_deinit();

    return;
}

/**
 * gnutls_rnd:
 * @level: a security level
 * @data: place to store random bytes
 * @len: The requested size
 *
 * This function will generate random data and store it to output
 * buffer. The value of @level should be one of %GNUTLS_RND_NONCE,
 * %GNUTLS_RND_RANDOM and %GNUTLS_RND_KEY. See the manual and
 * %gnutls_rnd_level_t for detailed information.
 *
 * This function is thread-safe and also fork-safe.
 *
 * Returns: Zero on success, or a negative error code on error.
 *
 * Since: 2.12.0
 **/
int gnutls_rnd(gnutls_rnd_level_t level, void *data, size_t len)
{
    FAIL_IF_LIB_ERROR;

    if (likely(len > 0)) {
        void *ctx = rnd_getctxt();
        if(ctx != NULL)
            return _gnutls_rnd_ops.rnd(ctx, level, data,
                                       len);
    }
    return 0;
}

/**
 * gnutls_rnd_refresh:
 *
 * This function refreshes the random generator state.
 * That is the current precise time, CPU usage, and
 * other values are input into its state.
 *
 * On a slower rate input from /dev/urandom is mixed too.
 *
 * Since: 3.1.7
 **/
void gnutls_rnd_refresh(void)
{
    if (_gnutls_rnd_ops.rnd_refresh)
    {
        void *ctx = rnd_getctxt();
        if(ctx != NULL)
            _gnutls_rnd_ops.rnd_refresh(ctx);
    }
}
